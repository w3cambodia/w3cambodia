@extends('backend.layouts.main')

@section('content')
    
<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Main content -->
      <section class="content">
        <div class="row">
          <div class="col-xs-12">
            <!-- Horizontal Form -->
            <div class="box box-info">
              
              {!! Form::open(['url' => 'backend/user_group','class'=>'form-horizontal']) !!}
              	<div class="with-border box-header">
	               <h3 class="box-title">New Group</h3>
	               
                    <div class="btn-group pull-right" role="group">
                                <a href="{{ url('backend/user_group') }}" class="btn btn-default btn-sm btn-savepage">Cancel</a>
                                {!! Form::submit('Enregistrer', ['class' => "btn btn-success btn-sm btn-savepage"]) !!}
                    </div>
	              </div><!-- /.box-header -->
	              <!-- form start -->
	             
                <div class="panel-box">  	
                    <div class="form-group">
                      
                    </div>
                    
                    <div class="col col-md-12">
						
						{!! Form::checkbox('is_admin', null, null, ['class' => 'l']) !!}
                        {!! Form::label('is_admin', 'Is Admin Group', ['class' => 'control-label']) !!}
						
					</div>
                    <div class="col col-lg-6">
                       	<div class="form-group">
                        	{!! Form::label('slug', 'Slug:', ['class' => 'control-label']) !!}
                           	{!! Form::text('slug', null, ['class' => 'form-control']) !!}
                      	</div>
                   </div>
                   <div class="col col-lg-6">
                       	<div class="form-group">
                        	{!! Form::label('level', 'Level:', ['class' => 'control-label']) !!}
                           	{!! Form::text('level', null, ['class' => 'form-control']) !!}
                      	</div>
                   </div>
                    
                    
                    <ul class="nav nav-tabs">
                        <li class="active"><a href="#en" data-toggle="tab">English</a></li>
                        <li ><a href="#kh" data-toggle="tab">Khmer</a></li>
                        <li ><a href="#fr" data-toggle="tab">French</a></li>
                    </ul>
            		<div class="tab-content">
	           			<div class="tab-pane fade active in" id="en">
                            <div class="row">
                                <div class="col col-lg-8">
                                    <div class="form-group">
                                        {!! Form::label('name_en', 'Name:', ['class' => 'control-label']) !!}
                                        {!! Form::text('name_en', null, ['class' => 'form-control']) !!}
                                    </div>
                                </div>
                        
                                <div class="col col-md-12">
                                    <div class="form-group">
                                        {!! Form::label('description_en', 'Description:', ['class' => 'control-label']) !!}
                                        {!! Form::textarea('description_en', null, ['class' => 'form-control ckeditor']) !!}
                                    </div>
                                </div>    
                            </div>
                       	</div>{{--end tab en--}}
						<div class="tab-pane fade" id="kh">
                            <div class="row">
                                <div class="col col-lg-8">
                                    <div class="form-group">
                                        {!! Form::label('name_kh', 'Name:', ['class' => 'control-label']) !!}
                                        {!! Form::text('name_kh', null, ['class' => 'form-control']) !!}
                                    </div>
                                </div>
                        
                                <div class="col col-md-12">
                                    <div class="form-group">
                                        {!! Form::label('description_kh', 'Description:', ['class' => 'control-label']) !!}
                                        {!! Form::textarea('description_kh', null, ['class' => 'form-control ckeditor']) !!}
                                    </div>
                                </div>    
                            </div>
                       	</div>
                       
                       	<div class="tab-pane fade" id="fr">
                            <div class="row">
                                <div class="col col-lg-8">
                                    <div class="form-group">
                                        {!! Form::label('name_fr', 'Name:', ['class' => 'control-label']) !!}
                                        {!! Form::text('name_fr', null, ['class' => 'form-control']) !!}
                                    </div>
                                </div>
                        
                                <div class="col col-md-12">
                                    <div class="form-group">
                                        {!! Form::label('description_fr', 'Description:', ['class' => 'control-label']) !!}
                                        {!! Form::textarea('description_fr', null, ['class' => 'form-control ckeditor']) !!}
                                    </div>
                                </div>    
                            </div>
                       </div>
                   	</div>
                    
                    
                </div><!-- /.box-body -->
                
              {!! Form::close() !!}
            </form>
            </div><!-- /.box -->
          
          </div>
        </div><!-- /.row -->
      </section><!-- /.content -->
  </div><!-- /.content-wrapper -->
  
  
  
@endsection