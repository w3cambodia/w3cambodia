@extends('backend.layouts.main')

@section('content')

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <span class="box-title" style="float:right;"><button class="btn btn-block btn-primary" onclick="location.href ='{{url('/backend/user_group/create')}}';"><i class="fa fa-wa fa-plus"></i> Add New</button></span>
      
      <h3>
        User Group
        
      </h3>
             
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">

          <div class="box">
            <div class="box-header">

             
            </div><!-- /.box-header -->
            <div class="box-body">
              <table class="table table-striped">
                
                  <tr> 
                    <th>No</th>
                    <th>Name</th>
                    
                    <th>Action</th>
                  </tr>
               
              
                <?php $i=1;?>
                @foreach ($roles as $role)
                
                  <tr>
                    <td>{{$i}}</td>
                    <td>{{$role->name_en}}</td>
                    
                    <td>
                      	<a href="{{ route('backend.user_group.show', $role->id) }}" class="btn btn-xs btn-primary btn-generique">Permission</a> &nbsp; 
						<a href="{{ route('backend.user_group.edit', $role->id) }}" class="btn btn-xs btn-primary btn-generique">Edit</a> &nbsp; 
						@if($role->id>7)
                        	<a href="#" class="btn btn-danger btn-xs btn-generique" data-toggle="modal" data-target="#deleteModal" 
		data-optionsmodal = "{{ $role->id }} | Delete this Group ? | {{ $role->name_en }} | group">Delete</a>
        				@endif
                    </td>
                  </tr>
                  <?php $i++;?>
				@endforeach
                  
              </table>
            </div><!-- /.box-body -->
          </div><!-- /.box -->
        </div><!-- /.col -->
      </div><!-- /.row -->
    </section><!-- /.content -->
  </div><!-- /.content-wrapper -->

@endsection

