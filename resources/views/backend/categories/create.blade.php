@extends('backend.layouts.main')

@section('content')

{!! Form::open(['route'=>'backend.categories.store', 'class'=> '']) !!}	

@include('backend.categories.form')

{!! Form::close() !!}	



@endsection