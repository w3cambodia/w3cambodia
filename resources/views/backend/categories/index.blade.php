@extends('backend.layouts.main') @section('content')

<div class="row">
    <div class="col col-lg-6">
    <h1>Category List</h1>
    </div>
    {{-- <div class="col col-lg-6">
    <a href="{{ Route('backend.categories.create') }}" class="btn btn-lg btn-primary pull-right">Add new category</a>
    </div> --}}
</div>

<table class="table table-striped">
    <tr>
        <th>Title</th>
        <th>KH</th>
        <th>FR</th>
        <th>Icon</th>
        <th>Actions</th>
    </tr>
    @foreach ($categories as $category)
    <tr>
        <td>{{ $category->title_en }}</td>
        <td>{{ $category->title_kh }}</td>
        <td>{{ $category->title_fr }}</td>
        <td><i class="fa fa-fw {{ $category->icon }}"></i></td>
        <td><a href="{{ route('backend.categories.edit', $category->id) }}" class="btn btn-xs btn-primary btn-generique">Edit</a>
        <a href="#" class="btn btn-danger btn-xs btn-generique" data-toggle="modal" data-target="#deleteModal" 
        data-optionsmodal = "{{ $category->id }} | Delete this Categorie ? | {{ $category->title_en }} | categories" 
        >Delete</a>
        </td>
    </tr>
    @endforeach

</table>

@include('backend.partials.modal')

@endsection

@section('bottomscripts')
<script src = "{!!getenv('_SITE_SUB_DOMAINE')!!}//assets/backend/js/www-cambodia.js"></script>
@endsection
