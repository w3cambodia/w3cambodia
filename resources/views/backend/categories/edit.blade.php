@extends('backend.layouts.main')

@section('content')

{!! Form::model($categories,[
'route'=>['backend.categories.update', $categories->id],
'method' => 'PATCH'
]) !!}
@include('backend.categories.form')

{!! Form::close() !!}	


@endsection