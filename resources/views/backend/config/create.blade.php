@extends('backend.layouts.main')

@section('content')
    
<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">


    <!-- Main content -->
      <section class="content">
        <div class="row">
          <div class="col-xs-12">
            <!-- Horizontal Form -->
            <div class="box box-info">
              
              {!! Form::open(['url' => 'backend/configs','class'=>'form-horizontal']) !!}
              	<div class="with-border box-header">
	               <h3 class="box-title">New Config</h3>
	               <div class="pull-right">
		               <span>
			               <button class="btn btn-success" type="submit">
			               		<i class="fa fa-wa fa-save"></i> Save 
			               </button> &nbsp;&nbsp; 
			               <a class="btn btn-default" href ="{{url('backend/configs')}}">
				               	<i class="fa fa-wa fa-reply"></i> Back to List
			               </a> 
		               </span>
	               	</div>
	              </div><!-- /.box-header -->
	              <!-- form start -->
	              
	               
                <div class="box-body">
                    
                    <div class="form-group">
                      <label  class="col-sm-4 control-label">Config Group<span class="validate_label_red">*</span></label>
                      <div class="col-sm-4">
                      	{!! Form::select('config_group_id', App\Models\Backend\ConfigGroup::lists('name', 'id'), NULL, ['class' => 'form-control'] ) !!}
	                    
                      </div>
                    </div>

                    <div class="form-group">
                      <label  class="col-sm-4 control-label">Name<span class="validate_label_red">*</span></label>
                      <div class="col-sm-4">
                        {!! Form::text('name',null,['class'=>'form-control','placeholder'=>'Config Name']) !!}
                      </div>
                    </div>

                    <div class="form-group">
                      <label  class="col-sm-4 control-label">Keyword<span class="validate_label_red">*</span></label>
                      <div class="col-sm-4">
                        {!! Form::text('code',null,['class'=>'form-control','placeholder'=>'Keyword']) !!}
                      </div>
                    </div>

                    <div class="form-group">
                      <label  class="col-sm-4 control-label">Value<span class="validate_label_red">*</span></label>
                      <div class="col-sm-4">
                        {!! Form::text('value',null,['class'=>'form-control','placeholder'=>'value']) !!}
                      </div>
                    </div>        
                    
                    
                </div><!-- /.box-body -->
                
              {!! Form::close() !!}
            </form>
            </div><!-- /.box -->
          
          </div>
        </div><!-- /.row -->
      </section><!-- /.content -->
  </div><!-- /.content-wrapper -->
  

@endsection