@extends('backend.layouts.main')

@section('content')

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="pull-right">
             	<span>
             		<button class="btn btn-primary" onclick="location.href ='{{url('backend/configs/create')}}';">
             			<i class="fa fa-wa fa-plus">
             			</i> Add New
             		</button>
             	</spa>
      </div>
      <h3>
        Config Management
      </h3>
      
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">

          <div class="box">
            
            
            <div class="box-body">
              <table id="example1" class="table table-bordered table-striped">
                <thead>
                  <tr>
                    <th>No.</th>
                   
                    <th>Config Group</th>
                    <th>Name</th>
                    <th>Keywords</th>
                    <th>Value</th>
                    <th>Action</th>
                  </tr>
                </thead> 
                <tbody>
                	<?php $i = 1;?>
                  <?php   
                  //print_r($alldata); exit;               
	                foreach($alldata as $data){
	                ?>
	                  <tr>
	                  	<td width="50"><?php echo($i); ?></td>
	                   
	                    <td>{{ $data->cg_name }}</td>
                      <td>{{ $data->name}} </td>
	                    
	                    <td>{{ $data->code }}</td>
                      <td>{{ $data->value }}</td>
	                    
	                    <td width="250">
	                      <a href="{{route('backend.configs.edit',$data->id)}}" class="btn btn-primary" title="Edit"><i class="fa fa-pencil"></i></a>
	                    </td>
	                    
	                  </tr>
					       <?php $i++; ?>
	              <?php  }?>
                </tbody>
                <!--<tfoot>
                  <tr>
                    <th>Rendering engine</th>
                    <th>Browser</th>
                    <th>Platform(s)</th>
                    <th>Engine version</th>
                    <th>CSS grade</th>
                  </tr>
                </tfoot>-->
              </table>
            </div><!-- /.box-body -->
          </div><!-- /.box -->
        </div><!-- /.col -->
      </div><!-- /.row -->
    </section><!-- /.content -->
  </div><!-- /.content-wrapper -->

@endsection

