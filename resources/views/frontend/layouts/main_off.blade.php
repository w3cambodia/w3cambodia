<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>www-Cambodia</title>

    <!-- Bootstrap Core CSS -->
    <link href="{!!getenv('_SITE_SUB_DOMAINE')!!}/assets/frontend/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="{!!getenv('_SITE_SUB_DOMAINE')!!}/assets/frontend/css/modern-business.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="{!!getenv('_SITE_SUB_DOMAINE')!!}/assets/frontend/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

    <!-- CSS www-cambodia-->
    <link href="{!!getenv('_SITE_SUB_DOMAINE')!!}/assets/frontend/css/www-cambodia.css" rel="stylesheet">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
    @yield('topscripts')

</head>

<body>

    <!-- Navigation -->
  @include('frontend.partials.navbar')

    <!-- Page Content -->
    <div class="container">
        @include('flash::message')
        @include('backend.partials.errors')
    </div>
    @yield('content')
    <!-- /.container -->

    <!-- jQuery -->
    <script src="{!!getenv('_SITE_SUB_DOMAINE')!!}/assets/frontend/js/jquery.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="{!!getenv('_SITE_SUB_DOMAINE')!!}/assets/frontend/js/bootstrap.min.js"></script>

    <!-- Script to Activate the Carousel -->

@yield('bottomscripts')
<script>
    $('#flash-overlay-modal').modal();
</script>

</body>

</html>