@extends('frontend.layouts.main')
@section('bigPicture')
<div class="row">
<div class="col col-lg-12">
            <div class="container">
               <img src="{!!getenv('_SITE_SUB_DOMAINE')!!}/assets/frontend/images/img2.jpg" alt="" class="col-lg-12">
            </div>
</div>
</div>
@endsection

@section('content')
<h1>{{$category_title}}</h1>
<hr />

<?php for($i=1;$i<sizeof($eventList);$i++){?>
<div class="row">
	<div class="col-lg-4 col-sm-4 col-sx-12">
		<a href="{!!getenv('_SITE_SUB_DOMAINE')!!}/event/{{ $eventList[$i]->slug }}"><img src="{{ _SITE_SUB_DOMAINE.$eventList[$i]->pictureEvent }}" style="border:solid 1px #eee; padding:0px;" alt="{{$eventList[$i]->title }}" class="col-lg-12" /></a>
	</div > 
    <div class="col-lg-8 col-sm-8 col-sx-12">
    	<h3><a href="{!!getenv('_SITE_SUB_DOMAINE')!!}/event/{{ $eventList[$i]->slug }}">{{$eventList[$i]->title }}</a></h3>
        <p>{{ str_limit($eventList[$i]->description, $limit = 100, $end = '...') }}</p>
    </div>  
</div>
 
<?php }?>
@endsection