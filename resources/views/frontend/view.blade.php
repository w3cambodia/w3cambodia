@extends('frontend.layouts.main')

@section('content')

<div class="container">
@foreach($event as $ev)	
	<!-- Page Heading/Breadcrumbs -->
	<div class="row">
	    <h1>{{$ev->title}}</h1>
	</div>
	
	<div class="row">
	    <div class="col-lg-4 col-sm-4 col-sx-12">
        	<img src="{{ _SITE_SUB_DOMAINE.$ev->pictureEvent }}" style="padding:0px;" class="clo-lg-12 col-sm-12 col-sx-12" alt="{{$ev->title}}" />
		</div>
        <div class="col-lg-8 col-sm-8 col-sx-12">
        	Event When: <b>{{$ev->eventWhen}}</b><br />
            Event End: <b>{{$ev->when_end}}</b><br />
            Event Where: <b>{{$ev->eventWhere}}</b><br />
            
        </div>
        <div class="col-lg-12 col-sm-12 col-sx-12">
        	{{$ev->description}}
        </div>
    </div>
</div>
@endforeach

{!! Form::open(['url' => 'event/comment','class'=>'form-horizontal']) !!}
<h3>Comment</h3>
<hr />
<?php if($comments){?>
@foreach($comments as $comment)	
	<div class="row">
	    <div class="col-lg-2 col-sm-2 col-sx-3">
        	<img src="{{ _SITE_SUB_DOMAINE.$comment->avatar }}" class="clo-lg-12 col-sm-12 col-sx-12" alt="" /><br />
            <b>{{$comment->name}}</b><br />
            {{$comment->created_at}}
        </div>
		<div class="col-lg-10 col-sm-10 col-sx-9">
        	<p>{{$comment->message}}</p>
            
        </div>
        
    </div>
@endforeach
<?php }else{?>
	<div class="row">
	    <div class="col-lg-12 col-sm-12 col-sx-12">
        	There are no comment yet.
        </div>
    </div>
<?php }?>
<?php if(isset(Auth::User()->id)){?>
	<div class="row">
	    <div class="col-lg-12 col-sm-12 col-sx-12">
            <div>
                <label  class="control-label">Comment: </label><br />
                <div class="col-sm-12">
                    {!! Form::hidden('created_by',Auth::User()->id,['class'=>'form-control','placeholder'=>'Enter your comment here!']) !!}
                    {!! Form::hidden('slug',$event[0]->slug,['class'=>'form-control','placeholder'=>'Enter your comment here!']) !!}
                    {!! Form::hidden('event_id',$event[0]->id,['class'=>'form-control','placeholder'=>'Enter your comment here!']) !!}
                    {!! Form::textarea('message',null,['class'=>'form-control col-lg-12','placeholder'=>'Enter your comment here!']) !!}
                </div>
            </div>
            
		</div>
        <div><br /><br /><button class="btn btn-success" type="submit">Submit</button></div>
	</div>
<?php }?>
	<hr>

<!-- Footer -->
@include('frontend.partials.footer')

@endsection