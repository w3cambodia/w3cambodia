@extends('frontend.layouts.main')


@section('bigPicture')
<div class="row">
<div class="col col-lg-12">
	<div class="container">
       	@include('frontend.modules.top_winner_image')	
               
 	</div>
</div>
</div>
@endsection

@section('content')

<div class="row">
	@include('frontend.modules.top_winner')	
</div>
<div class="row">
    @include('frontend.modules.top_like')	
</div>

        <hr>

        <!-- Footer -->
        <footer>
            <div class="row">
                <div class="col-lg-12">
                    <p>Copyright &copy; 2016. All Right Reserved WWW-CMBODIA. Power by sgkhmer</p>
                </div>
            </div>
        </footer>
@endsection