<div class="prenavbar">
    <div class="container ">
    <ul class="nav navbar-navy">
    	{{-- <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                @if(session()->has('cityActive'))
                {{ session('cityActive') }}
                @else
                Phnom Penh 2
                @endif

            <b class="caret"></b></a>
            <ul class="dropdown-menu">
                <li>
                    <a href="{{ route('setCity', 'Phnom Penh') }}">Phnom Penh</a>
                </li>
                <li>
                    <a href="{{ route('setCity', 'Siem Reap') }}">Siem Reap</a>
                </li>
                <li>
                    <a href="{{ route('setCity', 'Sihanoukville') }}">Sihanoukville</a>
                </li>
            </ul>
        </li> --}}
        
        <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                {{ Config::get('languages')[App::getLocale()] }}
                <b class="caret"></b>
            </a>
            <ul class="dropdown-menu">
                @foreach (Config::get('languages') as $lang => $language)
                    @if ($lang != App::getLocale())
                        <li>
                            {!! link_to_route('lang.switch', $language, $lang) !!}
                        </li>
                    @endif
                @endforeach
            </ul>
        </li>

            @if(Auth::User())
        <li class="dropdown">
        <a href="#" class="dropdown-toggle" data-toggle="dropdown">{{ Auth::User()->name }} 
        <img src="{{ Auth::User()->avatar }}" alt="" class="avatarImg">
        <b class="caret"></b></a>
            <ul class="dropdown-menu">
        <li><a href="{{ route('profile', Auth::User()->idcrypt) }}">Profil</a></li>
        @if(Auth::User()->hasRole('admin|superadmin'))
            <li><a href="{!!getenv('_SITE_SUB_DOMAINE')!!}/backend">Admin Site</a></li>
            @endif
        <li><a href="{!!getenv('_SITE_SUB_DOMAINE')!!}/auth/logout">Logout</a></li>
            </ul>
        </li>
        @else
        <li><a href="{!!getenv('_SITE_SUB_DOMAINE')!!}/backend">Login</a></li>
        @endif

    </ul>
    </div>
</div>