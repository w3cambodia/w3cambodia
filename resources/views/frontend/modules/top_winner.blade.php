<h1 class="page-header">Hot for today</h1>
<?php if(sizeof($topEvent)>0){?>			
<?php for($i=0;$i<sizeof($topEvent);$i++){?>
<div class="col-md-4">
	<div class="panel panel-default">
   		<div class="panel-heading headingTopFiveAdd" style="background-image:url('{{ _SITE_SUB_DOMAINE.$topEvent[$i]->pictureEvent }}')">
        	<a href="{!!getenv('_SITE_SUB_DOMAINE')!!}/event/{{ $topEvent[$i]->slug }}"><h3>{{ $topEvent[$i]->title }}</h3>
           	<h4 class="whereTransparent{{ $topEvent[$i]->category_id }}">
     			{{ $topEvent[$i]->eventWhere }}
           	</h4></a>
   		</div>
        <div class="panel-body bodyTopFiveAdd">
      		<!--<p>{{ str_limit($topEvent[$i]->description, $limit = 150, $end = '...') }}</p>-->
      		<div class="topFiveBottom">
        		<div class=" category{{ $topEvent[$i]->category_id }} topFiveBottomLine"></div>
                <div class="bottomLogoPosition">
               		<a href="{!!getenv('_SITE_SUB_DOMAINE')!!}/events/cat/{{ $topEvent[$i]->category_id }}"<span><i class="fa fa-fw {{ $topEvent[$i]->icon }} category{{ $topEvent[$i]->category_id }} bottomLogo"></i></span></a>
             	</div>
        	</div>
     	</div>
	</div>
</div>
<?php }?>
<?php }else{?>
<div class="col-md-4">
  <div class="panel panel-default">
      <h3>There no event today!</h3>
  </div>
</div>
<?php }?>