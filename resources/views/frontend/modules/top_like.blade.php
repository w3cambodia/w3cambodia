<?php if(sizeof($topLike)>0){?>
<h1 class="page-header">Top List</h1>
			
<?php for($i=0;$i<sizeof($topLike);$i++){?>
<div class="col-md-4">
	<div class="panel panel-default">
   		<div class="panel-heading headingTopFiveAdd" style="background-image:url('{{ _SITE_SUB_DOMAINE.$topLike[$i]->pictureEvent }}')">
        	<a href="{!!getenv('_SITE_SUB_DOMAINE')!!}/event/{{ $topLike[$i]->slug }}"><h3>{{ $topLike[$i]->title }} ({{$topLike[$i]->likes}})</h3>
           	<h4 class="whereTransparent{{ $topLike[$i]->category_id }}">
     			{{ $topLike[$i]->eventWhere }}
           	</h4></a>
            
            <!--<span class="pull-right">{{$topLike[$i]->likes}}</span>-->
   		</div>
        <div class="panel-body bodyTopFiveAdd">
      		<!--<p>{{ str_limit($topLike[$i]->description, $limit = 150, $end = '...') }}</p>-->
      		<div class="topFiveBottom">
        		<div class=" category{{ $topLike[$i]->category_id }} topFiveBottomLine"></div>
                <div class="bottomLogoPosition">
               		<a href="{!!getenv('_SITE_SUB_DOMAINE')!!}/events/cat/{{ $topLike[$i]->category_id }}"<span><i class="fa fa-fw {{ $topLike[$i]->icon }} category{{ $topLike[$i]->category_id }} bottomLogo"></i></span></a>
             	</div>
        	</div>
     	</div>
	</div>
</div>
<?php }?>
<?php }?>
