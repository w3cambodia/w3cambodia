<div class="navbar-default sidebar" role="navigation">
    <div class="sidebar-nav navbar-collapse">
        <ul class="nav" id="side-menu">
            {{-- <li class="sidebar-search">
                <div class="input-group custom-search-form">
                    <input type="text" class="form-control" placeholder="Search...">
                    <span class="input-group-btn">
                        <button class="btn btn-default" type="button">
                            <i class="fa fa-search"></i>
                        </button>
                    </span>
                </div>
                <!-- /input-group -->
            </li> --}}
            <li>
                <a href="{!!getenv('_SITE_SUB_DOMAINE')!!}/sponsor/"><i class="fa fa-dashboard fa-fw"></i> Dashboard</a>
            </li>
      	<?php
		$role_id = Auth::User()->role_id;
		$menu = DB::table('role_detail')
                    ->join('backend_menus', 'role_detail.menu_code', '=', 'backend_menus.code')
					->where('role_detail.role_id',$role_id ) 
					->where('published','=',1) 
                    ->where('parent_id', '=', 0)
                    ->orderBy('ordering','ASC')
					->get();
                    //->toSql();
	   //print_r($menu);exit;
	   foreach ($menu as $m){
			
            $menu_name = $m->name;
            
            $menu_link = $m->menu_link;
            $menu_icon_class = $m->icon_class;
			
			$smenu1 = DB::table('role_detail')
					->join('backend_menus', 'role_detail.menu_code', '=', 'backend_menus.code')
					->where('role_detail.role_id',$role_id )
                    ->where('published',1) 
                    ->where('parent_id', $m->id)
                    ->orderBy('ordering','ASC')
                    ->get();
			echo '<li>';
			
			if(sizeof($smenu1)>0){
				echo '<a href="#"><i class="fa '.$menu_icon_class.'"></i> '.$menu_name.'</a>';
		?>
        		<ul class="nav nav-second-level">
         		<?php
				foreach ($smenu1 as $m1){
			
					$menu1_name = $m1->name;
					$menu1_link = $m1->menu_link;
					$menu1_icon_class = $m1->icon_class;
					
					$smenu2 = DB::table('role_detail')
							->leftjoin('backend_menus', 'role_detail.menu_code', '=', 'backend_menus.code')
							->where('role_detail.role_id',$role_id )
							->where('published','=',1) 
							->where('parent_id',$m1->id)
							->orderBy('ordering','ASC')
							->get();
					echo '<li>';
					if(sizeof($smenu2)>0){
						echo '<a href="#"><i class="fa '.$menu1_icon_class.'"></i> '.$menu1_name.'</a>';
					?>
                    	<ul class="nav nav-third-level">
                    <?php
						foreach ($smenu2 as $m2){
				
							$menu2_name = $m2->name;
							$menu2_link = $m2->menu_link;
							$menu2_icon_class = $m2->icon_class;
					?>	
                        	<li><a href="{{_SITE_SUB_DOMAINE._SITE_ADMINISTRATOR.$menu2_link}}"><i class="fa {{$menu2_icon_class}}"></i> {{$menu2_name}}</a></li>
                    <?php }?>
                    	</ul>
                    <?php
					}else{
				?>  
                	<a href="{{_SITE_SUB_DOMAINE._SITE_ADMINISTRATOR.$menu1_link}}"><i class="fa {{$menu1_icon_class}}"></i> {{$menu1_name}}</a>
                	
                <?php } //end else subm2?>
                	</li> 
                <?php }//end foreach subm1?>    
                </ul>
        <?php
				
			}//end if subm1
			else{
		?>
                <a href="{{_SITE_SUB_DOMAINE._SITE_ADMINISTRATOR.$menu_link}}"><i class="fa {{$menu_icon_class}}"></i> {{$menu_name}}</a>  
        <?php 
			}
			echo '</li>';
		}//end foreach?>
            
        </ul>
    </div>
    <!-- /.sidebar-collapse -->
</div>