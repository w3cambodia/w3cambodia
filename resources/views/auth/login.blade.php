@extends('backend.layouts.login')

@section('content')
<div class="content-wrapper">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h4 class="page-head-line">Please Login To Enter </h4>
            </div>

        </div>
        <div class="row">
            <div class="col-md-6">
               <h4> Login with facebook <strong> / </strong>Twitter :</h4>
                <br />
                <a href="/login/facebook" class="btn btn-social btn-facebook">
                        <i class="fa fa-facebook"></i>&nbsp; Facebook Account</a>
                &nbsp;OR&nbsp;
                <a href="/login/twitter" class="btn btn-social btn-google">
                        <i class="fa fa-google-plus"></i>&nbsp; Twitter</a>
             &nbsp;OR&nbsp;
                <a href="/login/google" class="btn btn-social btn-google">
                        <i class="fa fa-google-plus"></i>&nbsp; Google</a>
                <hr />
                 <h4> Or Login with your <strong>www-Cambodia account</strong></h4>
                <br />
           <form class="form-horizontal" role="form" method="POST" action="{{ url('/auth/login') }}">
                                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                 <label for="email">Enter Email : </label>
                    <input type="text" id="email" name="email" class="form-control" />
                    <label for="password">Enter Password :  </label>
                    <input type="password" id="password" name="password" class="form-control" />
                    <hr />
                    <button type="submit"><span class="glyphicon glyphicon-user"></span> &nbsp;Log Me In </button>&nbsp;
             </form>
            </div>
            <div class="col-md-6">
                <div class="alert alert-info">
                <div
                  class="fb-like"
                  data-share="true"
                  data-width="450"
                  data-show-faces="true">
                </div>
                    This is a free bootstrap admin template with basic pages you need to craft your project. 
                    Use this template for free to use for personal and commercial use.
                    <br />
                     <strong> Some of its features are given below :</strong>
                    <ul>
                        <li>
                            Responsive Design Framework Used
                        </li>
                        <li>
                            Easy to use and customize
                        </li>
                        <li>
                            Font awesome icons included
                        </li>
                        <li>
                            Clean and light code used.
                        </li>
                    </ul>
                   
                </div>
                <div class="alert alert-success">
                     <strong> Instructions To Use:</strong>
                    <ul>
                        <li>
                           Lorem ipsum dolor sit amet ipsum dolor sit ame
                        </li>
                        <li>
                             Aamet ipsum dolor sit ame
                        </li>
                        <li>
                           Lorem ipsum dolor sit amet ipsum dolor
                        </li>
                        <li>
                             Cpsum dolor sit ame
                        </li>
                    </ul>
                   
                </div>
            </div>

        </div>
    </div>
</div>
@endsection