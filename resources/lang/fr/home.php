<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Lignes de la page Home
    |--------------------------------------------------------------------------
    */

    'noEventsCategory' => "Il n'y a aucun évênements dans cette catégorie" ,
    'selectCityFirst'     => "Veuillez selectionner une ville",
    'eventsForTomorrow' => "Hep ! A ne pas rater. Nous avons déjà <strong> :combienEventTomorrow </strong> évênements inscrits pour demain. Revenez demain, ne les ratez pas. !",

];
