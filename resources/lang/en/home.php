<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Lignes de la page Home
    |--------------------------------------------------------------------------
    */

    'noEventsCategory' => 'There is no event in this category',
    'selectCityFirst'     => 'Please select a city first',
    'eventsForTomorrow' => "Hey ! Don't miss it, we already have <strong> :combienEventTomorrow </strong> events planned for tomorrow !",

];
