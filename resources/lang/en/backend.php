<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Lignes de la page Home
    |--------------------------------------------------------------------------
    */

    'deleteEvent' => 'Delete this Event ?',
    'reallyDeleteEvent' => "Really delete the event :nomEvent ?",
];
