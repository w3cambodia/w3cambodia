<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

        DB::statement('SET FOREIGN_KEY_CHECKS=0;');
        
        $this->call(WhatSeeder::class);
        $this->call(RolesSeeder::class);

        $this->call(UsersTableSeeder::class);
        $this->call(RolesUserSeeder::class);
        // $this->call(EventSeeder::class);
        $this->call(CitySeeder::class);

        DB::statement('SET FOREIGN_KEY_CHECKS=1;');

        Model::reguard();
    }
}
