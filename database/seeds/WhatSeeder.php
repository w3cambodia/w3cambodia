<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use App\What;

    class WhatSeeder extends Seeder {

        public function run()
        {
            DB::table('whats')->delete();
            $whats = [
                ['title_en' => 'Daily', 'title_kh'=> 'ជារៀងរាល់ថ្ងៃ', 'title_fr' =>  'Quotidien'],
                ['title_en' => 'Resto', 'title_kh'=> 'នៅសល់', 'title_fr' =>  'Resto'],
                ['title_en' => 'Music', 'title_kh'=> 'តន្ត្រី', 'title_fr' =>  'Musique'],
                ['title_en' => 'Week', 'title_kh'=> 'សប្តាហ៍', 'title_fr' =>  'Semaine'],
                ['title_en' => 'News', 'title_kh'=> 'ព័ត៍មាន', 'title_fr' =>  'Nouvelles']
            ];
            DB::table('whats')->insert($whats);
        }
    }

