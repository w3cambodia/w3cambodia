<?php

namespace App\Http\Requests\Admin;
use App\Http\Requests\Request;

//class PermissionRequest extends Request
class UserRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
	
            'username' => 'required|unique:user',
            'password' => 'required|confirmed|min:6',
            'email' => 'email|max:255|unique:user',
        	'group_id' => 'required'
        ];
    }
}
