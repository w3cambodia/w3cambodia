<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/
// \Carbon\Carbon::setLocale('en');


if(isset($_COOKIE['Language'])){
	session(['languageActive' => $_COOKIE['Language']]);
} else {
	session(['languageActive' => 'English']);
	setcookie("Language", 'English', time()+3600*24*365, '/');
}
//Route::group(array('middleware'=>['inconstruction', 'configsite']), function(){
/*Route::group(array('middleware'=>['configsite']), function(){

	Route::get('/', ['as' => 'home', "uses"=> "FrontendController@home"]);
	
	Route::get('/eve/{num}', ['as' => 'voirEvent', 'uses' => 'FrontendController@view']);
	Route::get('api/setCity/{city}', ['as' => 'setCity', 'uses' => 'FrontendController@setCity']);
	// Route::get('api/setLanguage/{language}', ['as' => 'setLanguage', 'uses' => 'FrontendController@setLanguage']);
	Route::get('lang/{lang}', ['as'=>'lang.switch', 'uses'=>'LanguageController@switchLang']);
	Route::post('api/storeEvent/{language}', ['as' => 'storeEvent', 'uses' => 'FrontendController@storeEvent']);
	Route::get('events/cat/{id}', ['as'=>'pageCat', 'uses'=>'Frontend\FrontendController@pageView']);
	
	Route::get('profile/{email}', ['as' => 'profile', 'uses' => 'ProfileController@edit']);
});
*/
//frontend route
Route::group(array('middleware' => ['configsite']), function(){
	Route::get('/', 'Frontend\FrontendController@home');
	Route::post('/event/comment', 'Frontend\FrontendController@comment');
	Route::get('/event/{num}', ['as' => 'viewEvent', 'uses' => 'Frontend\FrontendController@view']);
	
	Route::get('/eve/{num}', ['as' => 'voirEvent', 'uses' => 'FrontendController@view']);
	Route::get('api/setCity/{city}', ['as' => 'setCity', 'uses' => 'FrontendController@setCity']);
	// Route::get('api/setLanguage/{language}', ['as' => 'setLanguage', 'uses' => 'FrontendController@setLanguage']);
	Route::get('lang/{lang}', ['as'=>'lang.switch', 'uses'=>'LanguageController@switchLang']);
	Route::post('api/storeEvent/{language}', ['as' => 'storeEvent', 'uses' => 'FrontendController@storeEvent']);
	Route::get('events/cat/{id}', ['as'=>'pageCat', 'uses'=>'Frontend\FrontendController@pageView']);

	Route::get('agenda', 'Frontend\FrontendController@agenda');
	
	Route::get('profile/{email}', ['as' => 'profile', 'uses' => 'ProfileController@edit']);


});

Route::get('login', ['as'=>'login', 'uses' => function(){
	return view('auth.login');
}]);
Route::get('auth/logout', 'Auth\AuthController@getLogout');
Route::controllers([
    'auth' => 'Auth\AuthController',
    'password' => 'Auth\PasswordController',
]);

Route::get('login/{provider?}', 'Auth\AuthController@login');
//Route::get('auth/logout', 'Auth\AuthController@getLogout');

Route::group(['prefix' => 'backend', 'middleware'=>['auth','configsite']], function () {
	Route::get('home', function(){
	 	return view('backend.home');
	});
	
	Route::get('/', 'Backend\CheckRoleUser@index');

	Route::get('api/enregistrerChangeEvent', ['as' => 'enregistrerChangeEvent', 'uses' => 'BackendController@enregistrerChangeEvent']);

	Route::get('api/enregistrerChangeEnd', ['as' => 'enregistrerChangeEnd', 'uses' => 'BackendController@enregistrerChangeEnd']);

	Route::resource('events', 'Backend\EventController');
	Route::resource('categories', 'Backend\CategoriesController');
	Route::resource('tomoderate', 'ModerateController@index');
	Route::resource('whats', 'WhatController');
	

	Route::get('filemanager', function(){
		return view('backend.filemanager.index');
	});
	Route::get('calendar', function(){ return view('backend.events.calendar');});
	Route::get('tests', 'TestsController@index');
	
	//user
	Route::resource('users', 'Backend\UserController');
	Route::get('changePassword', ['as' => 'changePassword', 'uses' => 'UserController@changePassword']);
	Route::post('updatePassword', ['as' => 'updatePassword', 'uses' => 'UserController@updatePassword']);
	Route::resource('user_group', 'Backend\RoleController');
	Route::get('user_group/permission', 'Backend\RoleController@show');
	Route::post('role_update', 'Backend\RoleController@updateRolePermission');
	
	//config
	Route::resource('configs', 'Backend\ConfigController');
	Route::resource('config_group', 'Backend\ConfigGroupController');
	
});

Route::group(['prefix' => 'advertiser', 'middleware'=>['auth','configsite']], function () {
	Route::get('/advertiser/', function(){
	 	return view('advertiser.home');
	});
});

Route::group(['prefix' => 'sponsor', 'middleware'=>['auth','configsite']], function () {
	Route::get('/', function(){
	 	return view('sponsor.home');
	});
});

Route::group(['prefix' => 'partner', 'middleware'=>['auth','configsite']], function () {
	Route::get('/', function(){
	 	return view('partner.home');
	});
});

Route::get('/deploy', function () {
    // Artisan::call('gitpull:pull');
    // exec('cd /usr/share/nginx/html/', $output);
	$output = "test ici";
    // Discard any changes to tracked files since our last deploy
    // exec('git reset --hard HEAD', $output);

    // Update the local repository
    exec('git pull ssh://git@bitbucket.org/w3cambodia/w3cambodia.git', $output);
    echo 'PULL Request '.implode(' ', $output);
});