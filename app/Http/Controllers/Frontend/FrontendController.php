<?php
namespace App\Http\Controllers\Frontend;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;

use Carbon\Carbon as Carbon;
use App\Models\Frontend\Event;
use App\Models\Frontend\Comments;
use App\Models\Frontend\Categories;

//sreyleak
use DB;
use Session;
use Auth;
use Validator;
//end

class FrontendController extends Controller
{
    public function home(){
    	$language_code = Session::get('applocale');
		if($language_code=='') $language_code = _CONFIG_LANGUAGE;
        
		$topEvent = $this->sgTopWinner($language_code);
		$topEventImage = $this->sgTopWinnerImage($language_code);
        $topLike = $this->sgTopLike($language_code);
		
       	//dd($topEventImage);
        return view('frontend.home2', compact('topEvent'))
					->with('language_code',$language_code)
					->with('topEventImage',$topEventImage)
					->with('topLike',$topLike);
    }

    public function view($slug){
       	$language_code = Session::get('applocale');
		if($language_code=='') $language_code = _CONFIG_LANGUAGE;
	   	$event = $this->sgEventDetail($language_code,$slug);
		$comments = $this->sgGetComments($event[0]->id);
	   	//dd($event[0]->id);
       	return view('frontend.view', compact('event'))->with('comments',$comments);
    }
	
	public function comment(Request $request)
    {
        
        $input = $request->all();
		//dd($input);
        $validator = Validator::make($input, [
            'message' => 'required'
        ]);
        
        if ($validator->fails())
        {
            return redirect()->back()->withErrors($validator->errors());
        }else{

            Comments::create($input);

            return redirect('event/'.$input['slug'])->with('nmessage','Save Successfully');
        }		
    }

    public function setCity($city){
        session(['cityActive' => $city]);
        return redirect()->back();
    }

   
    public function pageView($id)
    {
		$language_code = Session::get('applocale');
		if($language_code=='') $language_code = _CONFIG_LANGUAGE;
        
		$category_title = $this->sgGetTitle('categories', $id, $language_code);
		$eventList = $this->sgGetEventByCategory($language_code,$id);
        
       	//dd($topEvent);
        return view('frontend.pagelist')->with('$language_code',$language_code)
										->with('eventList',$eventList)
										->with('category_title',$category_title);

        //return view('frontend.pagelist');
    }

    //agenda page
    public function agenda()
    {
        $language_code = Session::get('applocale');
        if($language_code=='') $language_code = _CONFIG_LANGUAGE;
        
        $title = 'Agenda';
        $eventList = $this->sgGetEventByAgenda($language_code);
        
        
        return view('frontend.agenda')->with('$language_code',$language_code)
                                        ->with('eventList',$eventList)
                                        ->with('title',$title);
 
    }
} 
