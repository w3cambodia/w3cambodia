<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;


//sreyleak
use DB;
use Session;
use Auth;
//end

abstract class Controller extends BaseController
{
    use DispatchesJobs, ValidatesRequests;

	public function sgGetTitle($table, $id, $language_code){
		$field = 'title_'.$language_code;
		$result = DB::table($table)
        ->select($field)
 		->where('id',$id)		
        ->get();
        //->toSql();
       
        if(empty($result)) $data = '';
        else $data = $result[0]->$field;
        return $data;
	}
	public function sgGetDescription($table, $id, $language_code){
		$field = 'description_'.$language_code;
		$result = DB::table($table)
        ->select($field)
 		->where('id',$id)		
        ->get();
        //->toSql();
       
        
        return $result;
	}
	public function sgEventDetail($language_code,$slug){
		
		$result = DB::table('events')
					->join('categories', 'categories.id', '=', 'events.category_id')
        			->select('events.*', "events.title_$language_code as title", "events.description_$language_code as description", "categories.title_$language_code as category_title","icon")
					->where('events.approved',1)
					->where('slug',$slug)		
					->orderBy('eventWhen', 'desc')
        			->get();
        		//->toSql();
       
        
        return $result;
	}
	public function sgGetLastestEvent($language_code){
		
		$result = DB::table('events')
					->join('categories', 'categories.id', '=', 'events.category_id')
        			->select('events.*', "events.title_$language_code as title", "events.description_$language_code as description", "categories.title_$language_code as category_title","icon")
					->where('events.approved',1)
					//->where('eventWhen','>=','now()')		
					->orderBy('eventWhen', 'desc')
					->limit(5)
        			->get();
        		//->toSql();
       
        
        return $result;
	}
	public function sgTopWinner($language_code){
		
		$result = DB::table('events')
					->join('categories', 'categories.id', '=', 'events.category_id')
        			->select('events.*', "events.title_$language_code as title", "events.description_$language_code as description", "categories.title_$language_code as category_title","icon")
					->where('events.approved',1)
					->where('events.is_winner',1)
					->whereDate('eventWhen', '=', date('Y-m-d'))
					->orderBy('last_bid_amount', 'desc')
					->limit(5)
        			->get();
        			//->toSql();
       //dd($result);
        
        return $result;
	}
	public function sgTopWinnerImage($language_code){
		
		$result = DB::table('events')
					//->join('categories', 'categories.id', '=', 'events.category_id')
        			->select('events.pictureEvent as image', "events.title_$language_code as title")
					->where('events.approved',1)
					->where('events.is_winner',1)
					->whereDate('eventWhen', '=', date('Y-m-d'))
					->orderBy('last_bid_amount', 'desc')
					->limit(5)
        			->get();
        			//->toSql();
       //dd($result);
        
        return $result;
	}
	public function sgTopLike($language_code){
		
		$result = DB::table('events')
					->join('categories', 'categories.id', '=', 'events.category_id')
        			->select('events.*', "events.title_$language_code as title", "events.description_$language_code as description", "categories.title_$language_code as category_title","icon")
					->where('events.approved',1)
					->where('eventWhen', '>=', date('Y-m-d').' 00:00:00')
					->orderBy('last_bid_amount', 'desc')	
					->orderBy('likes', 'desc')
					->limit(_MAX_TOP_LIKE)
        			->get();
        		//->toSql();
       
        
        return $result;
	}
	public function sgGetEventByCategory($language_code,$id){
		
		$result = DB::table('events')
					->join('categories', 'categories.id', '=', 'events.category_id')
        			->select('events.*', "events.title_$language_code as title", "events.description_$language_code as description", "categories.title_$language_code as category_title","icon")
					->where('events.approved',1)
					->where('events.category_id',$id)
					->whereDate('eventWhen', '=', date('Y-m-d'))	
					->orderBy('eventWhen', 'desc')
					->limit(5)
        			->get();
        		//->toSql();
       
        
        return $result;
	}
	public function sgGetComments($event_id){
		
		$result = DB::table('comments')
					->join('users', 'comments.created_by', '=', 'users.id')
					->select('comments.*', "users.name", "users.avatar")
					->where('comments.event_id',$event_id)
					->where('comments.published',1)		
					->orderBy('comments.created_at', 'desc')
					//->limit(5)
        			->get();
        		//->toSql();
       
        
        return $result;
	}

	//sgGetEventByAgenda
	public function sgGetEventByAgenda($language_code){
		
		$rows = DB::table('events')					
        			->select('date_formart(eventWhen, "%Y-%m-%d") as date')
					->where('events.approved',1)
					->whereDate('eventWhen', '>=', date('Y-m-d'))	
					->orderBy('eventWhen', 'ASC')					
        			->get();
        		//->toSql();
        $list = array();
        foreach($rows as $row){
        	$date  = $row->eventWhen;
        	$result = DB::table('events')					
        			->select("events.title_$language_code as title", "events.description_$language_code as description", "categories.title_$language_code as category_title","icon")
					->where('events.approved',1)
					//->where('events.category_id',$id)
					->whereDate('eventWhen', '=', $date)	
					->orderBy('last_bid_amount', 'desc')	
					->orderBy('likes', 'desc')
					//->limit(5)
        			->get();
        	$list[]=array('date'=>$date, 'events'=>$result);
       	}
       	dd($list) ;
        return $list; 

	}
}
