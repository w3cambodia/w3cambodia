<?php

namespace App\Http\Controllers\backend;

use Illuminate\Http\Request;
use App\Models\Backend\Categories;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Http\Requests\CreateWhatRequest;

class CategoriesController extends Controller
{
    public function index()
    {
        $categories = Categories::all();
        return view('backend.categories.index', compact('categories'));
    }

    public function create()
    {
        return view('backend.categories.create');
    }

    public function store(CreateCategoriesRequest $request)
    {
        $what = Categories::create($request->all());
        return redirect()->route('backend.categories.index');
    }

    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        $categories = Categories::findOrFail($id);
        return view('backend.categories.edit', compact('categories'));
    }

    public function update(Request $request, $id)
    {
        $what = Categories::findOrFail($id);
        $what->update($request->all());

        return redirect()->route('backend.categories.index');
      
    }

    public function destroy($id)
    {
        $categories = Categories::findOrFail($id);
        $categories->delete();
        
        return redirect()->route('backend.categories.index');
    }
}
