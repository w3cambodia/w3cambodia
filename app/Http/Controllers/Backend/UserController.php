<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;

use App\Models\Backend\User;

use App\Http\Requests;
use App\Http\Requests\CreateUserRequest;
use App\Http\Requests\UpdatePasswordRequest;
use App\Http\Controllers\Controller;

use Bican\Roles\Models\Role;
use DB;

class UserController extends Controller
{

    public function changePassword(Request $request){
        $user = User::findOrFail($request['id']);
        return view('backend.users.newpassword', compact('user'));
    }

    public function updatePassword(UpdatePasswordRequest $request){
       $user = User::findOrFail($request['id']);
       $user->password = bcrypt($request['password']);
       $user->save();
       flash()->success('Password successfully changed !');
       return redirect()->route('backend.users.index');
    }

    public function index()
    {
        //$users = User::paginate(_LIMIT_LIST_PER_PAGE);
		$alldata = DB::table('users')
        ->join('roles', 'roles.id', '=', 'users.role_id')
        ->select('users.*', 'roles.name_en as role_name')
        ->orderBy('users.id', 'desc')
        ->get();
        return view('backend.users.index') ->with('users',$alldata);
    }

    public function create()
    {
       return view('backend.users.create');
    }


    public function store(CreateUserRequest $request)
    {
        $user = User::Create($request->all());
        $user->password = bcrypt($request['password']);
        $user->save();
        if(isset($request['role'])){
        $user->detachAllRoles();
        $user->attachRole($request['role']);
        };
        flash()->success('User successfully created !');
        return redirect()->route('backend.users.index');
    }

    public function show($id)
    {
        //
    }


    public function edit($id)
    {
        $user = User::findorFail($id);
       return view('backend.users.edit', compact('user'));
        //
    }


    public function update(Request $request, $id)
    {
        $user = User::findorFail($id);
        $user->update($request->all());
        if(isset($request['password'])){
        $user->password = bcrypt($request['password']);
        }
        $user->save();
        if(isset($request['role'])){
        $user->detachAllRoles();
        $user->attachRole($request['role']);
        };
        flash()->success('User successfully saved !');
        return redirect()->route('backend.users.index');
    }

    public function destroy($id)
    {
        if($id>7){
            $user = User::findorFail($id);
            $user->delete();
            flash()->success('User successfully deleted !');
        }else{
             flash()->success('You have no permission to deleted !');
        }
        return redirect()->route('backend.users.index');
    }
}
