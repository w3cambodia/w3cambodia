<?php

namespace App\Http\Controllers\Backend;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Models\Backend\Event;
use App\Models\Backend\Language;
use App\Models\Backend\Categories;
use App\Http\Requests;
use App\Http\Requests\CreateEventRequest;
use App\Http\Controllers\Controller;

use DB;
use Validator;
use Auth;
use Session;

// use \Laracasts\Flash;

class EventController extends Controller
{
     /*public function __construct()
    {
       $this->middleware('auth');
       $menu_code = 's41';
       Session::flash('permissionOn_Menu_ID',$menu_code);
    }*/
	/**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    { 
        // $events = Event::where('city_id', '=', session('cityActive'))->paginate(10);
        $todayEvents = Event::Eventtoday()->paginate(20);
        $tomorrowEvents = Event::Eventtomorrow()->paginate(20);
        $nextEvents = Event::Eventnext()->paginate(20);
        return view('backend.events.index', compact('todayEvents', 'tomorrowEvents', 'nextEvents'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        $Lists = DB::table('categories')
        //->where('language_id',_CONFIG_LANGUAGE)
        ->select('id', 'title_en')
        ->orderBy('title_en')
        ->get();
        $categories = array();
        foreach ($Lists as $pl) {
            $categories[$pl->id] = $pl->title_en;
        }
        //dd($categpries);
       
       
        //$languages = Language::all();
		return view('backend.events.create')->with('categories',$categories)
        								//->with('languages',$languages)
        								->with('view_title','Create new')
										->with('action',"Create");
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  Request  $request
     * @return Response
     */
	 
    public function store(CreateEventRequest $request)
    {
        $request->position =="" ? $position = 0 : $position = $request->position;
        $request->likes =="" ? $likes = 0 : $likes = $request->likes;
        // dd($request->position);
        // dd($request->pictureEvent);
        //$created_by =  \Auth::user()->id;

        if($request->title_kh=='') $request->title_kh = $request->title_en;
        if($request->description_kh=='') $request->description_kh = $request->title_en;

        if($request->title_fr=='') $request->title_fr = $request->title_en;
        if($request->description_fr=='') $request->description_fr = $request->title_en;

        $request->approved == 'on' ? $approvedVal = 1: $approvedVal = 0;
		$request->is_winner == 'on' ? $is_winner = 1: $is_winner = 0; 
        $levent = Event::create([
            'published_at' => $request->published_at . ':00',
            'eventWhen' => $request->eventWhen . ':00',
            'when_end' => $request->when_end . ':00',
            'eventWhere' => $request->eventWhere,
            'eventWho' => $request->eventWho,
            'title_en'=>$request->title_en,
            'description_en'=>$request->description_en,
             'title_kh'=>$request->title_kh,
            'description_kh'=>$request->description_kh,
             'title_fr'=>$request->title_fr,
            'description_fr'=>$request->description_fr,
            'category_id'=>$request->category_id,
            'pictureEvent'=>$request->pictureEvent,
            'position'=>$position,
            'likes'=>$likes,
            'city_id' => $request->city,
            'approved' => $approvedVal,
			'is_winner' =>$is_winner,
            'created_by' => \Auth::user()->id
            ]);
        // Flash::error('Sorry! Please try again.');
        flash()->success('Event successfully created !');
        return redirect()->route('backend.events.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        $event = Event::findOrFail($id);
        return view('backend.events.edit', compact('event'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Request  $request
     * @param  int  $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        $event = Event::findOrFail($id);
        // if($request->approved=='on') {dd('valeur est a existe');} else {dd('valeur vide');};
        
        
        // $event->update($request->all());
        $request->approved=='on' ? $approvedVal = 1: $approvedVal = 0;
		$request->is_winner=='on' ? $is_winner = 1: $is_winner = 0;
        // if(isset($request->approved)) {$approvedVal = 1;} else {$approvedVal = 0;};
        // $approvedVal = 1;
        // if(empty($request->when_end)){
        //     $pieces = explode('T', $request->eventWhen);
        //     $when_end = $request->eventWhen
        // }
        $event->update([
            'published_at' => $request->published_at . ':00',
            'eventWhen' => $request->eventWhen . ':00',
            'when_end' => $request->when_end,
            'eventWhere' => $request->eventWhere,
            'eventWho' => $request->eventWho,
            'title_en'=>$request->title_en,
            'description_en'=>$request->description_en,
             'title_kh'=>$request->title_kh,
            'description_kh'=>$request->description_kh,
             'title_fr'=>$request->title_fr,
            'description_fr'=>$request->description_fr,
            'pictureEvent' => $request->pictureEvent,
             'position'=>$request->position,
            'likes'=>$request->likes,
            'city_id' => $request->city,
            'category_id'=>$request->category_id,
            'approved' =>$approvedVal,
			'is_winner' => $is_winner,
			'last_bid_amount'=>$request->last_bid_amount,
            ]);
        flash()->success('Event successfully updated !');
        return redirect()->route('backend.events.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        // dd($id);
      $event = Event::findOrFail($id);
      $event->delete();
      flash()->success('Event successfully deleted !');
      return redirect()->route('backend.events.index');
    }
}
