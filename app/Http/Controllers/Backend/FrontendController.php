<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Carbon\Carbon as Carbon;
use App\Event;
use App\Categorie;
use App\Http\Requests\CreateEventRequest;

class FrontendController extends Controller
{
    public function home(){
    	
         //    $eventsDWs = Event::Whattitle('Daily')->Whattitle('Week')->get();

        	// $eventsRMs = Event::Whattitle('Resto')->Whattitle('Music')->get();

        	// $eventsNs = Event::Whattitle('News')->get();
            // Utilisation d'un SCOPE event.php pour trouver l'add qui gagne
            // $winnerAdd = Event::AddNumberOne();
            $topFiveAdds = Event::TopFiveAdds();
            // $topFiveAdds = array_values($topFiveAdds);

            // foreach ($topFiveAdds as $topFiveAdd) {
            //     echo $topFiveAdd->title_en . ' ' .$topFiveAdd->what_id. '<br>';
            // };
            $lesCat = [];
             foreach ($topFiveAdds as $key => $topFiveAdd) {
                if (!in_array($topFiveAdd->what_id, $lesCat)) {
                $lesCat[] = $topFiveAdd->what_id;
                } else {
                 unset($topFiveAdds[$key]);
                }
            };
            $topFiveAdds = $topFiveAdds->values()->all();
            // dd($topFiveAdds);
            // foreach ($topFiveAdds as $topFiveAdd) {
            //     echo $topFiveAdd->title_en . ' ' .$topFiveAdd->what_id. '<br>';
            // };



            // dd($lesCat);
        // return view('frontend.home2', compact('eventsDWs', 'eventsRMs', 'eventsNs'));
            if(sizeof($topFiveAdds)<5) 
            {
             return view('frontend.noEvents');
            }
            // dd(sizeof($topFiveAdds));
        	return view('frontend.home2', compact('topFiveAdds'));
    }

    public function view($slug){
       $event = Event::where('slug', '=', $slug)->first();
       // dd($event->slug);
       return view('frontend.view', compact('event'));
    }


    public function setCity($city){
        session(['cityActive' => $city]);
        return redirect()->back();
    }

    // public function setLanguage($language){
    //     setcookie("Language", $language, time()+3600, '/');
    //     session(['languageActive' => $language]);
    //     // $locale = 'en';
    //     if($language=='English') { Session(['locale' => 'en']); };
    //     if($language=='French') { Session(['locale' => 'fr']); };
    //     if($language=='Khmer') { Session(['locale' => 'kh']); };
    //     // Session(['locale' => 'de']);
    //     // dd(Session::get('locale'));
    //     \App::setLocale('fr');
    //     $lang = 'fr';
    //             if ($lang != null) \App::setLocale($lang);
    //     // app()->setLocale('fr');
    //     // \App::setLocale('ertr');
    //     // \App::setLocale($locale);
    //     return redirect()->back();
    // }

    public function storeEvent(CreateEventRequest $request){
       $levent = Event::create([
            'published_at' => $request->published_at . ':00',
            'eventWhen' => $request->eventWhen . ':00',
            'where' => $request->where,
            'title'=>$request->title,
            'content'=>$request->content,
            'categorie_id'=>$request->categorie_id,
            'city_id' => $request->city,
            'created_by' => 1
            ]);
        // Flash::error('Sorry! Please try again.');
        // flash()->success('Event successfully created !');
           flash()->overlay('Your event will be published soon <br> En fait il est déjà publié. Regarde dans la liste.', 'Thank you');
        return redirect()->route('home');
    }

    public function pageView($id)
    {


        return view('frontend.pagelist');
    }
}
