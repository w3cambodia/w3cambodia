<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Models\Backend\Role;
use App\Http\Controllers\Controller;
use Auth;
use DB;
use Session;

class RoleController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
        //$this->middleware('guest');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $roles = Role::paginate(_LIMIT_LIST_PER_PAGE);
		return view('backend.role.role_list')->with('roles',$roles);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('backend.role.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $input = $request->all();
		 
		Role::create($input);
		//##########Set Event for ActivityLog############
		//$eventName = 'create';
		//Session::flash('eventName',$eventName);
		//$this->ActivityLog();
		
		 return redirect('backend/user_group')->with('message','Save Successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $role = Role::find($id);
		

		return view('backend.role.role_permission')->with('group_role',$role);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
       $role = Role::find($id);
		
		return view('backend.role.edit')->with('role',$role)
									  ->with('action',"Edition");
    }
	

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $input = $request->all();
        $project = Role::find($id);
        $project->update($input);

        //##########Set Event for ActivityLog############
        //$eventName = 'update';
        //Session::flash('eventName',$eventName);
        //$this->ActivityLog();

        return redirect('/backend/user_group')->with('message','Save Successfully');  
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
	
	public function updateRolePermission(Request $request){

		if(!$request->has('chk_read')){
			$request->offsetSet('chk_read',"0");
		}

		//##########Set Event for ActivityLog############
        //$eventName = 'update permission';
        ////Session::flash('eventName',$eventName);
        //$this->ActivityLog();

		
		$data = $request->all();
        $id = $data['id'];
		
        $del_record = DB::table('role_detail')->where('role_id', '=', $id)->delete();

         if($del_record>=0){
			if (isset($data['menu_code']) && is_array($data['menu_code'])){
	        	//$j=1;
	        	foreach ( $data['menu_code'] as $key=>$value )
				{	
					$write_arr='';
					//Write
					if(!empty($data['chk_write'])){
						for($i=0;$i<count($data['chk_write']);$i++){
							if($data['menu_code'][$key]==$data['chk_write'][$i]){
								$write_arr =1;
								break;
								//echo $data['menu_code'][$key]."---Match---".$read_arr."<br/>";
							}else{
								$write_arr =0;
								//echo $data['menu_code'][$key]."---Not Match---0".$read_arr."<br/>";
							}
						}
					}else{
						$write_arr =0;
					}

					//Read
					for($i=0;$i<count($data['chk_read']);$i++){
						if($data['menu_code'][$key]==$data['chk_read'][$i]){
							$read_arr =1;
							break;
							//echo $data['menu_code'][$key]."---Match---".$read_arr."<br/>";
						}else{
							$read_arr =0;
							//echo $data['menu_code'][$key]."---Not Match---0".$read_arr."<br/>";
						}
					}

					

					DB::table('role_detail')->insert(
					    [
						'role_id' => intval($id), 
					  	'menu_code' => $data['menu_code'][$key],
					  	'menu_id' => $data['menu_id'][$key],
					  	'parent_menu_id' => $data['parent_menu_id'][$key],
					  	'read' => intval($read_arr),
					  	'write' => intval($write_arr)
					    ]
					);
				}

				return redirect('/backend/user_group')->with('message','Save Successfully');
				
			 }
         }else{
          	return redirect('/backend/user_group');
         }
	}

}
