<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Requests\Backend\ConfigRequest;
use App\Http\Controllers\Controller;
use App\Models\Backend\Config;
use App\Models\Backend\ConfigGroup;
use Illuminate\Support\Facades\Input;

use DB;
use Validator;
use Auth;
use Session;

class ConfigController extends Controller
{
	
	public $view_title = "Config";
	

    public function __construct()
    {
       $this->middleware('auth');
       $menu_code = 'sub07';
       Session::flash('permissionOn_Menu_ID',$menu_code);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function index()
    {
    	
        //$results = Config::with('config_group')->get();
        //dd($results);
        $alldata = DB::table('configs')
        ->join('config_groups', 'config_groups.id', '=', 'configs.config_group_id')
        ->select('configs.*', 'config_groups.name as cg_name')
        ->orderBy('config_groups.id', 'asc')
        ->get();
        
        return view('backend.config.index')->with('alldata',$alldata);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
    	
        $config_group = ConfigGroup::lists('name','id');
		
        return view('backend.config.create')->with('config_group',$config_group)
        								->with('view_title',$this->view_title)
										->with('action',"Create");
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

    public function store(ConfigRequest $request)
    {
        
        $input = $request->all();

        $validator = Validator::make($input, [
            'name' => 'required',
            'config_group_id' => 'required',
            'code' => 'required|unique:configs',
            'value' => 'required'
        ]);
        
        if ($validator->fails())
        {
            return redirect()->back()->withErrors($validator->errors());
        }else{

            Config::create($input);

            //##########Set Event for ActivityLog############
            //$eventName = 'create';
            //Session::flash('eventName',$eventName);
            //$this->ActivityLog();
            	
            return redirect('backend/configs')->with('nmessage','Save Successfully');
        }		
    }

   
    public function edit($id)
    {
        
         
        $data = Config::find($id);
        
        
        
        return view('backend.config.edit')->with('config',$data)
										  ->with('action',"Edition");
    
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $input = $request->all();
        
        $project = Config::find($id);
        
        $validator = Validator::make($input, [
            'name' => 'required',
            'config_group_id' => 'required',
            'code' => 'required',
            'value' => 'required'
        ]);
        
        if ($validator->fails())
        {
            return redirect()->back()->withErrors($validator->errors());
        }else{

            $project->update($input);

            //##########Set Event for ActivityLog############
            //$eventName = 'update';
            //Session::flash('eventName',$eventName);
            //$this->ActivityLog();

            return redirect('/backend/configs')->with('message','Save Successfully');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //##########Set Event for ActivityLog############
        $eventName = 'delete';
        Session::flash('eventName',$eventName);
        $this->ActivityLog();
        //
        $data=UserModel::find($id)->delete();
        return redirect()->back()->with('message','Deleted successfully');
    }

}
