<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;

abstract class Controller extends BaseController
{
    use DispatchesJobs, ValidatesRequests;
	
	public function getTranslate($table, $field, $id, $language_id){
		
		$name = DB::table($table.'_translates')
        ->select($field)
        ->where('language_id',$language_id)
 		->where($table.'_id',$id)		
        ->get();
        //->toSql();
        //dd($language_id);
        //dd($name);
        if(empty($name)) $data = '';
        else $data = $name[0]->$field;
        return $data;
	}
	
}
