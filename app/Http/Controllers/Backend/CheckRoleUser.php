<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class CheckRoleUser extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // dd(\Auth::user()->role_id);
        if (\Auth::user()->role_id == 4) 
        {
            return \Redirect::to('/advertiser');
        }else if (\Auth::user()->role_id == 5) 
        {
            return \Redirect::to('/sponsor');
        }else if (\Auth::user()->role_id == 6) 
        {
            return \Redirect::to('/');
        }else if (\Auth::user()->role_id == 7) 
        {
            return \Redirect::to('/partner/');
        }else if (\Auth::user()->role_id == 1){
            return \Redirect::to('/backend/home');
        }else{
            //return \Redirect::to('/backend/home');
        }
    }

}
