<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\User;

use App\Http\Requests;
use App\Http\Requests\CreateUserRequest;
use App\Http\Requests\UpdatePasswordRequest;
use App\Http\Controllers\Controller;

use Bican\Roles\Models\Role;

class UserController extends Controller
{

    public function changePassword(Request $request){
        $user = User::findOrFail($request['id']);
        return view('backend.users.newpassword', compact('user'));
    }

    public function updatePassword(UpdatePasswordRequest $request){
       $user = User::findOrFail($request['id']);
       $user->password = bcrypt($request['password']);
       $user->save();
       flash()->success('Password successfully changed !');
       return redirect()->route('backend.users.index');
    }

    public function index()
    {
        $users = User::paginate(25);
        return view('backend.users.index', compact('users'));
    }

    public function create()
    {
       return view('backend.users.create');
    }


    public function store(CreateUserRequest $request)
    {
        $user = User::Create($request->all());
        $user->password = bcrypt($request['password']);
        $user->save();
        if(isset($request['role'])){
        $user->detachAllRoles();
        $user->attachRole($request['role']);
        };
        flash()->success('User successfully created !');
        return redirect()->route('backend.users.index');
    }

    public function show($id)
    {
        //
    }


    public function edit($id)
    {
        $user = User::findorFail($id);
       return view('backend.users.edit', compact('user'));
        //
    }


    public function update(Request $request, $id)
    {
        $user = User::findorFail($id);
        $user->update($request->all());
        if(isset($request['password'])){
        $user->password = bcrypt($request['password']);
        }
        $user->save();
        if(isset($request['role'])){
        $user->detachAllRoles();
        $user->attachRole($request['role']);
        };
        flash()->success('User successfully saved !');
        return redirect()->route('backend.users.index');
    }

    public function destroy($id)
    {
        $user = User::findorFail($id);
        $user->delete();
        flash()->success('User successfully deleted !');
        return redirect()->route('backend.users.index');
    }
}
