<?php
namespace App\Services;
use App\Models\Frontend\Categories;
use App\Models\Frontend\Event;
use Carbon\Carbon as Carbon;

class PlaceIcone
{
	public function icone($cat){

		$laCat = Categories::find($cat);
		$lesClasses = $laCat->icon ." category". $laCat->id;
		return $lesClasses;

	}

	public function makeListeCategories($lang)
	{
		//$lang = \Config::get('app.locale');
	    	$title = "title_".$lang." AS title";
		$categories = Categories::all($title, 'id', 'icon');
		return $categories;
	}
	
}