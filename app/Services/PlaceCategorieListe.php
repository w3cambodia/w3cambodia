<?php
namespace App\Services;
use App\Categorie;
use App\Event;
use Carbon\Carbon as Carbon;

class PlaceCategorieListe
{
	public function listeCategorie($cat){
		return Event::Whattitle($cat)->get();
	}

	public function combienEventDemain($cats)
	{
		$combien = 0;
		foreach ($cats as $cat) {
			$combien = $combien + Event::Eventdemain($cat)->count();
		}
		return $combien;
	}
}