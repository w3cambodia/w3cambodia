<?php
namespace App\Services;
use App\Models\Frontend\Categories;
use App\Models\Frontend\Event;
use Carbon\Carbon as Carbon;

class PlaceCategoryList
{
	public function listeCategory($cat){
		return Event::Categorytitle($cat)->get();
	}

	public function combienEventDemain($cats)
	{
		$combien = 0;
		foreach ($cats as $cat) {
			$combien = $combien + Event::Eventdemain($cat)->count();
		}
		return $combien;
	}
}