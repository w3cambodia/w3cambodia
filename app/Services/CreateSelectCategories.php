<?php
namespace App\Services;
use App\Categorie;

class CreateSelectCategories
{
	public function lescategories(){
		$categoriesSelect = Categorie::lists('title', 'id');
		return $categoriesSelect;
	}
}