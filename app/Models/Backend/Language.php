<?php namespace App\Models\Admin;

use Illuminate\Database\Eloquent\Model;

class Language extends Model {

	protected $table = 'languages';
	
	protected $fillable = ['name', 
						    'image',						   
						    'code',
						    'ordering'
							
	];

	//public $timestamps = false;
	
}
 