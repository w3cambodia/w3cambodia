<?php

namespace App\Models\Backend;

use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
	/*protected $table = 'role'
	protected $fillable = ['title', 'order_level'];
    protected $hidden = ['password', 'remember_token'];*/
	protected $fillable = ['name_en','name_kh','name_fr',
						   'description_en','description_kh','description_fr',
						   'level',
						   'slug',
						   'is_admin'];
    protected $table = 'roles';
    public function users()
    {
        return $this->hasMany('App\Models\Backend\User', 'role_id', 'id');
    }
}
