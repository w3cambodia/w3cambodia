<?php namespace App\Models\Admin;

use Illuminate\Database\Eloquent\Model;

class Passenger extends Model {

	//
	protected $table = 'passenger';
	protected $guarded  = [];
	public $timestamps = false;
}
