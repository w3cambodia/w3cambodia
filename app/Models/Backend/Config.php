<?php

namespace App\Models\Backend;

use Illuminate\Database\Eloquent\Model;

class Config extends Model
{
	protected $table = 'configs';
	protected $fillable = ['config_group_id', 
						    'name',
							'code',
							'value'							
	];
	
	public $timestamps = false;
	public function config_group(){
		return $this->belongsTo('App\Models\Backend\ConfigGroup','config_group_id','id');
		 //return $this->hasMany('App\ConfigGroup');
	}
}
