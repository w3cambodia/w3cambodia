<?php

namespace App\Models\Backend;

use Illuminate\Database\Eloquent\Model;

class ConfigGroup extends Model
{
	protected $table = 'config_groups';
	protected $fillable = ['name'];
	
	public $timestamps = false;//to avoid updated_at and created_at in default laravel framwork
}
