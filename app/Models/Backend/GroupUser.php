<?php namespace App\Models\Backend;

use Illuminate\Database\Eloquent\Model;

class GroupUser extends Model {

	protected $table = 'group_user';
	
	protected $fillable = ['name',
							
							'status',
							'remark',
							'create_by_id'
							];

	public $timestamps = false;
	
	
	
	public function user(){
		return $this->belongsTo('App\Models\Backend\UserModel','create_by_id');
	}
	
}
