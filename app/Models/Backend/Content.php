<?php

namespace App\Models\Admin;

use Illuminate\Database\Eloquent\Model;

class Content extends Model
{
	protected $table = 'content';
	protected $fillable = ['image', 
	 					    'content_category_id',
	 					    'ordering',
							'is_active',
							'created_by',
							'updated_by',						
	];
	
	//public $timestamps = false;
	
	public function content_category(){
		return $this->hasmany('App\Models\Admin\content_category','content_category_id');
	}
	
}
