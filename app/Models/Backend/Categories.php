<?php

namespace App\Models\Backend;

use Illuminate\Database\Eloquent\Model;

class Categories extends Model
{
protected $fillable = ['title_en', 'title_kh', 'title_fr', 'icon'];

    public function event()
        {
            return $this->hasMany('App\Models\Backend\Event');
        }
}
