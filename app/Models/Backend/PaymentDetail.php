<?php namespace App\Models\Admin;

use Illuminate\Database\Eloquent\Model;

class PaymentDetail extends Model {

	//
	protected $table = 'payment_detail';
	protected $guarded  = [];
	public $timestamps = false;
}
