<?php

namespace App\Models\Frontend;

use Illuminate\Database\Eloquent\Model;

class City extends Model
{
   protected $table = 'cities';
   protected $fillable = ['city'];

   public function event()
        {
            return $this->hasMany('App\Models\Frontend\Event');
        }
   
}
