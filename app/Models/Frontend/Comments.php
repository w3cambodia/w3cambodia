<?php

namespace App\Models\Frontend;
use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon as Carbon;

use DB;


class Comments extends Model
{    
    protected $fillable= ['parent_id', 'event_id', 'rate', 'message', 'published', 'created_by', 'is_reported', 'reported_by'];

}
